FROM jboss/wildfly:8.2.0.Final

COPY ["target/testJEE.war", "/opt/jboss/wildfly/standalone/deployments/testJEE.war"]

# add admin user
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
