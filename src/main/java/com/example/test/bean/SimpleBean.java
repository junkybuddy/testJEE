package com.example.test.bean;

/**
 * 
 *
 */
public class SimpleBean {

	public String getEcho(String msg) {
		if ( msg==null )
			msg = "hello";
		return msg+"..."+msg+"..."+msg;
	}
	
	public String getEchoCiao() {
		return getEcho("ciao");
	}

}
