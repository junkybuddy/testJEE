package com.example.test.junit;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.example.test.bean.*;

public class JTestCase {

	@Test
	public void testEchoBean() {
		SimpleBean bean = new SimpleBean();
		String echo = bean.getEchoCiao();
		assertTrue( echo.equals("ciao...ciao...ciao") );
	}

}
