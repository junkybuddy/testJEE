package com.example.test.junit;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

public class ITJTestCase {

	@Test
	public void testEchoWeb() throws ClientProtocolException, IOException {
		String appHost = System.getProperty("webapp.host");
		if ( appHost==null )
			return;
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("http://"+appHost+":8080/testJEE/");

		CloseableHttpResponse indexPageResp = httpClient.execute(httpGet);
		assertEquals(200, indexPageResp.getStatusLine().getStatusCode());
		
		String page = EntityUtils.toString( indexPageResp.getEntity() );
		assertTrue( page.contains("ciao...ciao...ciao"));
	}

}
