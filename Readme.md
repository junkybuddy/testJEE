[![build status](https://gitlab.com/junkybuddy/testJEE/badges/master/build.svg)](https://gitlab.com/junkybuddy/testJEE/commits/master)  [![Coverage Status](https://coveralls.io/repos/bitbucket/fabio_santagostino/wildfly8-jdk7/badge.svg)](https://coveralls.io/bitbucket/fabio_santagostino/wildfly8-jdk7?branch=$CI_BUILD_REF)

# Hi all from Santa

Simple JEE web app used for test purposes. 

## Build, deploy and test steps
1. build maven project with:
```
$ mvn package
```
1. deploy **testJEE.war** to you favourite JEE application server ([Wildfly](http://wildfly.org/), [WAS Liberty](https://developer.ibm.com/wasdev/downloads/liberty-profile-using-non-eclipse-environments/), etc.)
1. point your browser to [http://*$HOST*:*$PORT*/testJEE/]() and see a welcome page